## Xiaomi Firmware Packages For Redmi 4A (rolex)

#### Downloads: [![DownloadSF](https://img.shields.io/badge/Download-SourceForge-orange.svg)](https://sourceforge.net/projects/yshalsager/files/Stable) [![DownloadAFH](https://img.shields.io/badge/Download-AndroidFileHost-brightgreen.svg)](https://www.androidfilehost.com/?w=files&flid=268046)

| ID | MIUI Name | Device Name | Codename |
| --- | --- | --- | --- |
| 319 | HM4A | Xiaomi Redmi 4A | rolex |
| 319 | HM4AGlobal | Xiaomi Redmi 4A Global | rolex |

### XDA Main Thread:
[Go here](https://forum.xda-developers.com/android/software-hacking/devices-yshalsager-t3741446)

#### by [Xiaomi Firmware Updater](https://github.com/XiaomiFirmwareUpdater)
#### Developer: [yshalsager](https://github.com/yshalsager)
